﻿DROP DATABASE IF EXISTS ejemplo2u3;
CREATE DATABASE IF NOT EXISTS ejemplo2u3;
USE ejemplo2u3;


/* Ej1 */

-- ej1a
DELIMITER //
CREATE OR REPLACE PROCEDURE ej1a(texto varchar(50),caracter char(1))
  BEGIN
    IF (POSITION(caracter IN texto)>0) THEN
      SELECT 'El caracter esta en el texto';
    ELSE
      SELECT 'El caracter no esta en el texto';
    END IF;
  END //
DELIMITER ;

CALL ej1a('Esta es una prueba','a');

-- ej1b
DELIMITER //
CREATE OR REPLACE PROCEDURE ej1b(texto varchar(50),caracter char(1))
  BEGIN
    IF (LOCATE(caracter,texto)<>0) THEN
      SELECT 'El caracter esta en el texto';
    ELSE
      SELECT 'El caracter no esta en el texto';
    END IF;
  END //
DELIMITER ;

CALL ej1b('Esta es una prueba','i');

/* Ej2 */
DELIMITER //
CREATE OR REPLACE PROCEDURE ej2 (texto varchar(50), caracter char(1))
  BEGIN
    SELECT SUBSTRING_INDEX(texto,caracter,1);
  END //                                                        
DELIMITER ;

CALL ej2('Este es una prueba','a');


/* Ej3 */
DELIMITER //
CREATE OR REPLACE PROCEDURE ej3 (n1 int, n2 int , n3 int,OUT mayor int,OUT menor int)
  BEGIN
    SET mayor = GREATEST(n1,n2,n3);
    set menor = LEAST(n1,n2,n3);
  END //
DELIMITER ;

CALL ej3(6,3,9,@vmayor,@vmenor);

SELECT @vmayor,@vmenor;

/* Ej4 */
DELIMITER //
  CREATE OR REPLACE PROCEDURE eje4()
    BEGIN
      DECLARE num1 int DEFAULT 0;
      DECLARE num2 int DEFAULT 0;
      DECLARE resultado int DEFAULT 0;
      
      SET num1 = (SELECT COUNT(*) FROM datos WHERE numero1>50);
      SET num2 = (SELECT COUNT(*) FROM datos WHERE numero2>50);

      SET resultado = num1+num2;

      SELECT resultado;
    END //
DELIMITER ;

CALL eje4();

/* Ej5 */
DELIMITER //
  CREATE OR REPLACE PROCEDURE eje5()
    BEGIN
      UPDATE 
        datos SET suma = numero1+numero2,
                  resta = IF(numero1<numero2,numero2-numero1,numero1-numero2);       
    END //
DELIMITER ;

CALL eje5();
SELECT * FROM datos;

/* Ej6 */
DELIMITER //
  CREATE OR REPLACE PROCEDURE ej6()
    BEGIN
      UPDATE 
        datos SET suma=IF(numero1>numero2,numero1+numero2,NULL),
                  resta=IF(numero2>numero1,numero2-numero1,numero1-numero2);            
    END //
DELIMITER ;

CALL ej6();

SELECT * FROM datos;

/* Ej7 */
DELIMITER //
  CREATE OR REPLACE PROCEDURE ej7()
    BEGIN
      UPDATE 
        datos 
          SET junto=CONCAT_WS(' ',texto1,texto2); 
    END //
DELIMITER ;

CALL ej7();

SELECT * FROM datos d;

/* Ej8 */
DELIMITER //
  CREATE OR REPLACE PROCEDURE ej8()
    BEGIN
      UPDATE 
        datos d 
          SET d.junto=IF(d.rango='A',CONCAT_WS('-',texto1,texto2),IF(d.rango='B',CONCAT_WS('+',texto1,texto2),d.texto1));
    END //
DELIMITER ;

CALL ej8();

SELECT * FROM datos d;

