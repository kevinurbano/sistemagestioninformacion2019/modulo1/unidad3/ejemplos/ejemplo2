﻿USE ejemplo2u3;

CREATE OR REPLACE TABLE datos (
 datos_id INT(11) NOT NULL AUTO_INCREMENT,
 numero1 INT(11) NOT NULL,
 numero2 INT(11) NOT NULL,
 suma VARCHAR(255) DEFAULT NULL,
 resta VARCHAR(255) DEFAULT NULL,
 rango VARCHAR(5) DEFAULT NULL,
 texto1 VARCHAR(25) DEFAULT NULL,
 texto2 VARCHAR(25) DEFAULT NULL,
 junto VARCHAR(255) DEFAULT NULL,
 longitud VARCHAR(255) DEFAULT NULL,
 tipo INT(11) NOT NULL,
 numero INT(11) NOT NULL,
 PRIMARY KEY (datos_id)
  )
ENGINE = INNODB,
CHARACTER SET latin1,
COLLATE latin1_swedish_ci,
COMMENT = 'Esta tabla esta para poder actualizarla con los procedimientos',
ROW_FORMAT = Compact,
TRANSACTIONAL = 0;
ALTER TABLE datos
 ADD INDEX numero2_index(numero2);
ALTER TABLE datos
 ADD INDEX numero_index(numero);
ALTER TABLE datos
 ADD INDEX tipo_index(tipo);


INSERT INTO datos ( numero1, numero2, suma, resta, rango, texto1, texto2, junto, longitud, tipo, numero)
  VALUES (10, 20, '', '', 'A', 'Hola', 'Mundo', '', '', 0, 0),
         (14, 19, '', '', 'B', 'Esta es una', 'Prueba', '', '', 0, 0),
         (8, 14, '', '', 'A', 'Uniendo texto1', 'texto2', '', '', 0, 0),
         (78, 94, '', '', 'B', 'un texto', 'otro texto', '', '', 0, 0),
         (75, 44, '', '', 'C', 'Que es', 'esto?', '', '', 0, 0);
